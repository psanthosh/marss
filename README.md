## Fedora Fastest Mirror Finder 

### A Simple Mirror Finder For Fedora Linux
- Version 0.3

####  Dependencies used:
- Library Web Qt
- Library qt-base
- qt5-qtbase-devel


##### How to Generate a `MakeFile`
- <u>copy the files to etc/yum.repos.d first! </u>
- <b>Open the terminal in etc/yum.repos.d and run </b> `qmake-qt5` 



###### How to Compile:
<p>
You just need to make MirrorFinder executable using command bellow :

`make`
</p>

#### To run:
- <b>Only run once and enjoy speed. ;)</b>

<p>Dont Forget To Make It Executable</p>

- `sudo ./MirroFinder`

### Note:
- <b> Make sure to use "sudo"</b>
- <b> The process can take a while, grab a cup of coffee and relax, you'll see "finish" output when done, you can use your PC
while it is testing around 100 fedora mirrors and 40 RPMFusion Mirrors to find the fastest mirror for you! </b>

### Summary:

- `qmake-qt5`
- `make`
- `sudo ./MirroFinder`

##### This Software Just Work for this Repositories :

<table style="width:100%">
  <tr>
    <th>Repository</th>
    <th>Repo detail</th>
  </tr>
  <tr>
    <td>Fedora</td>
    <td>Main Repository</td>
  </tr>
  <tr>
    <td>RpmFusion</td>
    <td>RpmFusion Free and RpmFuison Non-Free</td>
  </tr>
</table>

##################################################################
<p>
<b>contact me :</b>

`recive_mail_gitlab@riseup.net`
</p>

